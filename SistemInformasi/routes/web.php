<?php

use App\Http\Controllers\Dashboard;
use App\Http\Controllers\Home;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\MataKuliahController;
use App\Http\Controllers\RiwayatController;


Route::get('/', Home::class)->name('home');
Route::get('/dashboard', Dashboard::class)->name('dashboard')->middleware('auth');

Route::resources([
    'dosen' => DosenController::class,
    'mahasiswa' => MahasiswaController::class,
    'matakuliah' => MataKuliahController::class,
    'riwayat' => \App\Http\Controllers\RiwayatController::class,
    'kelas' => \App\Http\Controllers\KelasController::class,
    'dosenmatakuliah' => \App\Http\Controllers\DosenMataKuliahController::class,
]);

Route::get('/dosen/createriwayat/{id}', [DosenController::class, 'createriwayat']);
Route::post('/dosen/storeriwayat', [DosenController::class, 'storeriwayat']);

Route::get('/matakuliah/createkelas/{id}', [MataKuliahController::class, 'createkelas']);
Route::post('/matakuliah/storekelas', [MataKuliahController::class, 'storekelas']);

Route::get('/dosen/matakuliah/tambah/{matakuliah_id}/{dosen_id}',[DosenController::class, 'tambahmatakuliahdosen']);
Route::get('/dosen/matakuliah/hapus/{matakuliah_id}/{dosen_id}',[DosenController::class, 'hapusmatakuliahdosen']);

Route::get('/mahasiswa/kelas/tambah/{kelas_id}/{mahasiswa_id}',[MahasiswaController::class, 'tambahkelasmahasiswa']);
Route::get('/mahasiswa/kelas/hapus/{kelas_id}/{mahasiswa_id}',[MahasiswaController::class, 'hapuskelasmahasiswa']);
