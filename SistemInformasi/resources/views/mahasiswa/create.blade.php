@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Tambah Mahasiswa" />

    {!! form()->open(route('mahasiswa.store')) !!}
    @include('mahasiswa._form')
    {!! form()->close() !!}

@stop
