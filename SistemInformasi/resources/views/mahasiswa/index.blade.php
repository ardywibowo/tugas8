@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Mahasiswa">
        <x-item>
            <x-link label="tambah" icon="plus" url="{{ route('mahasiswa.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! Suitable::source($mahasiswa)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable('nama'),
            Laravolt\Suitable\Columns\Text::make('nim','nim')->sortable(),
            Laravolt\Suitable\Columns\Text::make('jenis_kelamin','jenis kelamin')->sortable(),
            Laravolt\Suitable\Columns\Text::make('ttl','tempat, tanggal lahir')->sortable(),
            Laravolt\Suitable\Columns\RestfulButton::make('mahasiswa', 'Action'),
        ])->render()
    !!}

@stop
