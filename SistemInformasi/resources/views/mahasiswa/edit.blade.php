@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Edit Mahasiswa" />

    {!! form()->bind($mahasiswa)->put(route('mahasiswa.update', $mahasiswa->id)) !!}
    @include('mahasiswa._form')
    {!! form()->close() !!}

@stop
