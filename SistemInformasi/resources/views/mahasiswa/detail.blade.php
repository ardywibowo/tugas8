@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Detail Mahasiswa"/>
    <div class="ui grid">
        <div class="row">
            <div class="two wide column">
                <strong>Nama</strong>
            </div>
            <div class="two wide column">{{$mahasiswa->nama}}</div>
        </div>
        <div class="row">
            <div class="two wide column">
                <strong>NIP</strong>
            </div>
            <div class="two wide column">{{$mahasiswa->nim}}</div>
        </div>
        <div class="row">
            <div class="two wide column">
                <strong>Jenis Kelamin</strong>
            </div>
            <div class="two wide column">{{$mahasiswa->jenis_kelamin}}</div>
        </div>
        <div class="row">
            <div class="two wide column">
                <strong>Tempat, Tanggal lahir</strong>
            </div>
            <div class="two wide column">{{$mahasiswa->ttl}}</div>
        </div>


    </div>
    <br>

    <h3 class="ui header">Kelas yang diambil</h3>
    {!! Suitable::source($sudah)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable(),
            ['header' => 'Action', 'raw' => function($rows){
                $val = '<a href="/mahasiswa/kelas/hapus/'.$rows->id.'/'.$rows->mahasiswa_id.'" class="ui button red mini icon secondary" data-tooltip="Hapus" data-position="top center">
                 <i aria-hidden="true" class="icon trash"></i></a>';
                return $val;
            }],
        ])->render()
    !!}

    <br>
    <h3 class="ui header">Kelas yang Tersedia</h3>
    {!! Suitable::source($belum)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable(),
            ['header' => 'Action', 'raw' => function($rows){
                $val = '<a href="/mahasiswa/kelas/tambah/'.$rows->id.'/'.$rows->mahasiswa_id.'" class="ui button green mini icon secondary" data-tooltip="Tambah" data-position="top center">
                 <i aria-hidden="true" class="icon plus"></i></a>';
                return $val;
            }],
        ])->render()
    !!}
@endsection
