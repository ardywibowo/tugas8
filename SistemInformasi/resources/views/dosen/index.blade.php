@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Dosen">
        <x-item>
            <x-link label="tambah" icon="plus" url="{{ route('dosen.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! Suitable::source($dosen)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable('nama'),
            Laravolt\Suitable\Columns\Text::make('nip','nip')->sortable(),
            Laravolt\Suitable\Columns\Text::make('gelar','gelar')->sortable(),
            Laravolt\Suitable\Columns\RestfulButton::make('dosen', 'Action'),
        ])->render()
    !!}

@stop
