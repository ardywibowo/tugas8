@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Tambah Dosen" />

    {!! form()->open(route('dosen.store')) !!}
    @include('dosen._form')
    {!! form()->close() !!}

@stop
