@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Tambah Riwayat" />

    {!! form()->open('/dosen/storeriwayat') !!}
    {!! form()->hidden('dosen_id', $id) !!}
    @include('dosen._formriwayat')
    {!! form()->close() !!}

@stop
