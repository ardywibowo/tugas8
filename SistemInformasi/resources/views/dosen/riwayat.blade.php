@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Detail Dosen"/>
    <div class="ui grid">
        <div class="row">
            <div class="two wide column">
                <strong>Nama</strong>
            </div>
            <div class="two wide column">{{$dosen->nama}}</div>
        </div>
        <div class="row">
            <div class="two wide column">
                <strong>NIP</strong>
            </div>
            <div class="two wide column">{{$dosen->nip}}</div>
        </div>
        <div class="row">
            <div class="two wide column">
                <strong>Gelar</strong>
            </div>
            <div class="two wide column">{{$dosen->gelar}}</div>
        </div>

    </div>
    <br>

    <h3 class="ui header">Riwayat Pendidikan Dosen</h3>
    <x-link label="tambah" icon="plus" url="{{ 'createriwayat/'.$dosen->id}}"></x-link>
    {!! Suitable::source($dosen->riwayat_pendidikan)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('strata','strata')->sortable(),
            Laravolt\Suitable\Columns\Text::make('jurusan','jurusan')->sortable(),
            Laravolt\Suitable\Columns\Text::make('sekolah','sekolah')->sortable(),
            Laravolt\Suitable\Columns\Text::make('tahun_mulai','tahun_mulai')->sortable(),
            Laravolt\Suitable\Columns\Text::make('tahun_selesai','tahun_selesai')->sortable(),
            Laravolt\Suitable\Columns\RestfulButton::make('riwayat', 'Action')->except('view'),
        ])->render()
    !!}

    <br>

    <h3 class="ui header">Mata Kuliah yang diambil</h3>
    {!! Suitable::source($sudah)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable(),
            Laravolt\Suitable\Columns\Text::make('sks','sks')->sortable(),
            ['header' => 'Action', 'raw' => function($rows){
                $val = '<a href="/dosen/matakuliah/hapus/'.$rows->id.'/'.$rows->dosen_id.'" class="ui button red mini icon secondary" data-tooltip="Hapus" data-position="top center">
                 <i aria-hidden="true" class="icon trash"></i></a>';
                return $val;
            }],
        ])->render()
    !!}

    <br>
    <h3 class="ui header">Mata Kuliah yang Tersedia</h3>
    {!! Suitable::source($belum)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable(),
            Laravolt\Suitable\Columns\Text::make('sks','sks')->sortable(),
            ['header' => 'Action', 'raw' => function($rows){
                $val = '<a href="/dosen/matakuliah/tambah/'.$rows->id.'/'.$rows->dosen_id.'" class="ui button green mini icon secondary" data-tooltip="Tambah" data-position="top center">
                 <i aria-hidden="true" class="icon plus"></i></a>';
                return $val;
            }],
        ])->render()
    !!}
@endsection
