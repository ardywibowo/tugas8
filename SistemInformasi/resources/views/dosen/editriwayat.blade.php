@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Edit Riwayat" />

    {!! form()->bind($riwayat)->put(route('riwayat.update', $riwayat->id)) !!}
    @include('dosen._formriwayat')
    {!! form()->close() !!}

@stop
