@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Edit Dosen" />

    {!! form()->bind($dosen)->put(route('dosen.update', $dosen->id)) !!}
    @include('dosen._form')
    {!! form()->close() !!}

@stop
