@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Tambah Mata Kuliah" />

    {!! form()->open(route('matakuliah.store')) !!}
    @include('matakuliah._form')
    {!! form()->close() !!}

@stop
