@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Edit Kelas" />

    {!! form()->bind($kelas)->put(route('kelas.update', $kelas->id)) !!}
    @include('matakuliah._formkelas')
    {!! form()->close() !!}

@stop
