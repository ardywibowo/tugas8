@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Detail Mata Kuliah"/>
    <div class="ui grid">
        <div class="row">
            <div class="two wide column">
                <strong>Nama</strong>
            </div>
            <div class="two wide column">{{$matakuliah->nama}}</div>
        </div>
        <div class="row">
            <div class="two wide column">
                <strong>Jumlah SKS</strong>
            </div>
            <div class="two wide column">{{$matakuliah->sks}}</div>
        </div>
    </div>
    <br>

    <h3 class="ui header">List Kelas</h3>
    <x-link label="tambah" icon="plus" url="{{ 'createkelas/'.$matakuliah->id}}"></x-link>
    {!! Suitable::source($matakuliah->kelas)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable(),
            Laravolt\Suitable\Columns\RestfulButton::make('kelas', 'Action')->except('view'),
        ])->render()
    !!}
@endsection
