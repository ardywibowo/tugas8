@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Edit Mata Kuliah" />

    {!! form()->bind($matakuliah)->put(route('matakuliah.update', $matakuliah->id)) !!}
    @include('matakuliah._form')
    {!! form()->close() !!}

@stop
