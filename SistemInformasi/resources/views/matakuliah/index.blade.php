@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Mata Kuliah">
        <x-item>
            <x-link label="tambah" icon="plus" url="{{ route('matakuliah.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! Suitable::source($matakuliah)->search()->columns([
            Laravolt\Suitable\Columns\Numbering::make('No'),
            Laravolt\Suitable\Columns\Text::make('nama','nama')->sortable('nama'),
            Laravolt\Suitable\Columns\Text::make('sks','jumlah sks')->sortable('sks'),
            Laravolt\Suitable\Columns\RestfulButton::make('matakuliah', 'Action'),
        ])->render()
    !!}

@stop
