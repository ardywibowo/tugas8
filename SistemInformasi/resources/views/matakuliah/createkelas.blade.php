@extends('laravolt::layouts.app')

@section('content')
    <x-titlebar title="Tambah Kelas" />

    {!! form()->open('/matakuliah/storekelas') !!}
    {!! form()->hidden('matakuliah_id', $id) !!}
    @include('matakuliah._formkelas')
    {!! form()->close() !!}

@stop
