<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Suitable\AutoSearch;
use Laravolt\Suitable\AutoSort;

class Kelas extends Model
{
    use AutoSearch,AutoSort;
    use HasFactory;
    protected $table = 'kelas';
    protected $searchableColumns = ['nama'];
    protected $fillable = ['nama','matakuliah_id'];

    public function mahasiswa()
    {
        return $this->belongsToMany('App\Models\Mahasiswa');
    }
    public function matakuliah()
    {
        return $this->belongsTo('App\Models\MataKuliah');
    }
}
