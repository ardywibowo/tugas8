<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Suitable\AutoSearch;
use Laravolt\Suitable\AutoSort;

class MataKuliah extends Model
{
    use AutoSearch,AutoSort;
    use HasFactory;
    protected $table = 'matakuliah';
    protected $searchableColumns = ['nama'];
    protected $fillable = ['nama','sks'];

    public function dosen()
    {
        return $this->belongsToMany('App\Models\Dosen');
    }
    public function kelas()
    {
        return $this->hasMany('App\Models\Kelas', 'matakuliah_id');
    }
}
