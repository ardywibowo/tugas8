<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Suitable\AutoSearch;
use Laravolt\Suitable\AutoSort;

class Riwayat extends Model
{
    use AutoSearch,AutoSort;
    use HasFactory;
    protected $table = 'riwayat_pendidikan';
    protected $searchableColumns = ['strata','jurusan','sekolah','tahun_mulai','tahun_selesai'];
    protected $fillable = ['strata','jurusan','sekolah','tahun_mulai','tahun_selesai','dosen_id'];

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }
}
