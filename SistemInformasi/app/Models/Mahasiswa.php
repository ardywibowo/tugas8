<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Suitable\AutoSearch;
use Laravolt\Suitable\AutoSort;

class Mahasiswa extends Model
{
    use AutoSearch,AutoSort;
    use HasFactory;
    protected $table = 'mahasiswa';
    protected $searchableColumns = ['nama','nim','ttl'];
    protected $fillable = ['nama','nim','jenis_kelamin','ttl'];

    public function kelas()
    {
        return $this->belongsToMany('App\Models\Kelas');
    }
}
