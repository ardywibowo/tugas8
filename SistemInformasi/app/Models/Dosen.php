<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Suitable\AutoSearch;
use Laravolt\Suitable\AutoSort;

class Dosen extends Model
{
    use AutoSearch,AutoSort;
    use HasFactory;
    protected $table = 'dosen';
    protected $searchableColumns = ['nama','nip','gelar'];
    protected $fillable = ['nama','nip','gelar'];

    public function riwayat_pendidikan()
    {
        return $this->hasMany('App\Models\Riwayat');
    }

    public function matakuliah()
    {
        return $this->belongsToMany('App\Models\MataKuliah');
    }
}
