<?php

namespace App\Http\Controllers;

use App\Http\Requests\MahasiswaStoreRequest;
use App\Http\Requests\MahasiswaUpdateRequest;
use App\Models\Mahasiswa;
use App\Models\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswa = Mahasiswa::autoSearch(\request('search'))->autoSort()->paginate(5);

        return view('mahasiswa.index', compact('mahasiswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MahasiswaStoreRequest $request)
    {
        Mahasiswa::create($request->except('_token'));

        return redirect()->route('mahasiswa.index')->withSuccess('Berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mahasiswa $mahasiswa)
    {
        $sudah = DB::select('select * from kelas where id = any (select kelas_id from kelas_mahasiswa where mahasiswa_id = :id)', ['id' => $mahasiswa->id]);
        foreach ($sudah as $s)
        {
            $s->mahasiswa_id = $mahasiswa->id;
        }
        $belum = DB::select('select * from kelas where id not in (select kelas_id from kelas_mahasiswa where mahasiswa_id = :id)', ['id' => $mahasiswa->id]);
        foreach ($belum as $b)
        {
            $b->mahasiswa_id = $mahasiswa->id;
        }
        return view('mahasiswa.detail', compact('mahasiswa','sudah','belum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Mahasiswa $mahasiswa)
    {
        return view('mahasiswa.edit', compact('mahasiswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MahasiswaUpdateRequest $request, Mahasiswa $mahasiswa)
    {
        $mahasiswa->update($request->except('_token'));
        return redirect()->route('mahasiswa.index')->withSuccess('Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mahasiswa $mahasiswa)
    {
        $kelas = DB::table('kelas_mahasiswa')->where('mahasiswa_id', $mahasiswa->id)->get();
        $jumlahkelas = count($kelas);
        if ($jumlahkelas > 0)
        {
            return redirect()->back()->withErrors('Mahasiswa masih memiliki kelas');
        }
        $mahasiswa->delete();

        return redirect()->back()->withSuccess('Berhasil dihapus');
    }

    public function tambahkelasmahasiswa($kelas_id,$mahasiswa_id)
    {
        $data = Mahasiswa::find($mahasiswa_id);
        $jumlah_sks = $data->jumlah_sks;
        $kelas = Kelas::find($kelas_id);
        $jumlah_sks = $jumlah_sks + $kelas->matakuliah->sks;
        if($jumlah_sks > 24)
        {
            return redirect()->route('mahasiswa.show', $mahasiswa_id)->withErrors('Jumlah SKS melebihi 24');
        }
        DB::table('mahasiswa')->where('id', $mahasiswa_id)->update([
            'jumlah_sks' => $jumlah_sks
        ]);
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => $kelas_id,
            'mahasiswa_id' => $mahasiswa_id
        ]);
        return redirect()->route('mahasiswa.show', $mahasiswa_id)->withSuccess('Berhasil ditambah');
    }

    public function hapuskelasmahasiswa($kelas_id,$mahasiswa_id)
    {
        $data = Mahasiswa::find($mahasiswa_id);
        $jumlah_sks = $data->jumlah_sks;
        $kelas = Kelas::find($kelas_id);
        $jumlah_sks = $jumlah_sks - $kelas->matakuliah->sks;
        DB::table('mahasiswa')->where('id', $mahasiswa_id)->update([
            'jumlah_sks' => $jumlah_sks
        ]);

        DB::table('kelas_mahasiswa')->where([
            ['kelas_id', $kelas_id],
            ['mahasiswa_id', $mahasiswa_id],
        ])->delete();

        return redirect()->route('mahasiswa.show', $mahasiswa_id)->withSuccess('Berhasil dihapus');
    }
}
