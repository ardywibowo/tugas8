<?php

namespace App\Http\Controllers;

use App\Http\Requests\DosenStoreRequest;
use App\Http\Requests\DosenUpdateRequest;
use App\Http\Requests\RiwayatStoreRequest;
use App\Models\Riwayat;
use Illuminate\Http\Request;
use App\Models\Dosen;
use Illuminate\Support\Facades\DB;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosen = Dosen::autoSearch(\request('search'))->autoSort()->paginate(5);

        return view('dosen.index', compact('dosen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DosenStoreRequest $request)
    {
        Dosen::create($request->except('_token'));

        return redirect()->route('dosen.index')->withSuccess('Berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Dosen $dosen)
    {
        $sudah = DB::select('select * from matakuliah where id = any (select matakuliah_id from dosen_matakuliah where dosen_id = :id)', ['id' => $dosen->id]);
        foreach ($sudah as $s)
        {
            $s->dosen_id = $dosen->id;
        }
        $belum = DB::select('select * from matakuliah where id not in (select matakuliah_id from dosen_matakuliah where dosen_id = :id)', ['id' => $dosen->id]);
        foreach ($belum as $b)
        {
            $b->dosen_id = $dosen->id;
        }
        return view('dosen.riwayat', compact('dosen','sudah','belum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Dosen $dosen)
    {
        return view('dosen.edit', compact('dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DosenUpdateRequest $request,Dosen $dosen)
    {
        $dosen->update($request->except('_token'));
        return redirect()->route('dosen.index')->withSuccess('Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dosen $dosen)
    {
        $riwayat = Riwayat::where('dosen_id',$dosen->id)->get();
        $jumlahriwayat = count($riwayat);
        if ($jumlahriwayat > 0)
        {
            return redirect()->back()->withErrors('Dosen masih memiliki riwayat');
        }
        $matakuliah = DB::table('dosen_matakuliah')->where('dosen_id', $dosen->id)->get();
        $jumlahmatakuliah = count($matakuliah);
        if ($jumlahmatakuliah > 0)
        {
            return redirect()->back()->withErrors('Dosen Masih memiliki Mata Kuliah');
        }
        $dosen->delete();

        return redirect()->back()->withSuccess('Berhasil dihapus');
    }

    public function createriwayat($id)
    {
        return view('dosen.createriwayat',compact('id'));
    }

    public function storeriwayat(RiwayatStoreRequest $request)
    {
        Riwayat::create($request->except('_token'));

        return redirect()->route('dosen.show', $request->dosen_id)->withSuccess('Berhasil disimpan');
    }

    public function tambahmatakuliahdosen($matakuliah_id,$dosen_id)
    {
        DB::table('dosen_matakuliah')->insert([
            'matakuliah_id' => $matakuliah_id,
            'dosen_id' => $dosen_id
        ]);

        return redirect()->route('dosen.show', $dosen_id)->withSuccess('Berhasil ditambah');
    }

    public function hapusmatakuliahdosen($matakuliah_id,$dosen_id)
    {
        DB::table('dosen_matakuliah')->where([
            ['matakuliah_id', $matakuliah_id],
            ['dosen_id', $dosen_id],
        ])->delete();

        return redirect()->route('dosen.show', $dosen_id)->withSuccess('Berhasil dihapus');
    }
}
