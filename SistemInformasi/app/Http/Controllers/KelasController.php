<?php

namespace App\Http\Controllers;

use App\Http\Requests\KelasUpdateRequest;
use App\Models\Kelas;
use App\Models\MataKuliah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::find($id);
        return view('matakuliah.editkelas', compact('kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KelasUpdateRequest $request,$id)
    {
        $kelas = Kelas::find($id);
        $kelas->update($request->except('_token'));
        return redirect()->back()->withSuccess('Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Kelas::find($id);
        $mahasiswa = DB::table('kelas_mahasiswa')->where('kelas_id', $id)->get();
        $jumlahmahasiswa = count($mahasiswa);
        if ($jumlahmahasiswa > 0)
        {
            return redirect()->back()->withErrors('Kelas masih dipilih mahasiswa');
        }
        $kelas->delete();

        return redirect()->back()->withSuccess('Berhasil dihapus');
    }
}
