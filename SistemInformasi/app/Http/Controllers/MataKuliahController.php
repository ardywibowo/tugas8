<?php

namespace App\Http\Controllers;

use App\Http\Requests\KelasStoreRequest;
use App\Http\Requests\MataKuliahStoreRequest;
use App\Http\Requests\MataKuliahUpdateRequest;
use App\Models\Kelas;
use App\Models\MataKuliah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matakuliah = MataKuliah::autoSearch(\request('search'))->autoSort()->paginate(5);

        return view('matakuliah.index', compact('matakuliah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('matakuliah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MataKuliahStoreRequest $request)
    {

        MataKuliah::create($request->except('_token'));

        return redirect()->route('matakuliah.index')->withSuccess('Berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MataKuliah $matakuliah)
    {
        return view('matakuliah.kelas', compact('matakuliah'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MataKuliah $matakuliah)
    {
        return view('matakuliah.edit', compact('matakuliah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MataKuliahUpdateRequest $request, MataKuliah $matakuliah)
    {
        $matakuliah->update($request->except('_token'));
        return redirect()->route('matakuliah.index')->withSuccess('Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MataKuliah $matakuliah)
    {
        $kelas = Kelas::where('matakuliah_id',$matakuliah->id)->get();
        $jumlahkelas = count($kelas);
        if ($jumlahkelas > 0)
        {
            return redirect()->back()->withErrors('Mata Kuliah Masih memiliki kelas');
        }
        $dosen = DB::table('dosen_matakuliah')->where('matakuliah_id', $matakuliah->id)->get();
        $jumlahdosen = count($dosen);
        if ($jumlahdosen > 0)
        {
            return redirect()->back()->withErrors('Matakuliah masih dipilih Dosen');
        }
        $matakuliah->delete();

        return redirect()->back()->withSuccess('Berhasil dihapus');
    }

    public function createkelas($id)
    {
        return view('matakuliah.createkelas',compact('id'));
    }

    public function storekelas(KelasStoreRequest $request)
    {
        Kelas::create($request->except('_token'));

        return redirect()->route('matakuliah.show', $request->matakuliah_id)->withSuccess('Berhasil disimpan');
    }
}
