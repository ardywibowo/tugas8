<?php

return [
    'Main Menu' => [
        'menu' => [
            'Dashboard' => [
                'url' => 'admin/dashboard',
                'data' => [
                    'icon' => 'chart line icon',
                    'roles' => 'admin',
                ],
            ],
            'Dosen' => [
                'url' => 'dosen',
                'data' => [
                    'icon' => 'circle outline',
                ],
            ],
            'Mahasiswa' => [
                'url' => 'mahasiswa',
                'data' => [
                    'icon' => 'circle outline',
                ],
            ],
            'Mata Kuliah' => [
                'url' => 'matakuliah',
                'data' => [
                    'icon' => 'circle outline',
                ],
            ],
        ],
    ],
];
